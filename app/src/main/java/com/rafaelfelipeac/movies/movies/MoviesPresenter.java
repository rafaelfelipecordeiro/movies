package com.rafaelfelipeac.movies.movies;

import android.annotation.SuppressLint;
import com.rafaelfelipeac.movies.model.MovieListResponse;
import com.rafaelfelipeac.movies.service.Constants;
import com.rafaelfelipeac.movies.service.MoviesService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class MoviesPresenter implements MoviesContract.Presenter {
    private MoviesService moviesService;
    private MoviesContract.View view;

    MoviesPresenter(MoviesService moviesService) {
        this.moviesService = moviesService;
    }

    @Override
    public void setView(MoviesContract.View view) {
        this.view = view;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getMovies() {
        moviesService.getMovies(new Constants().apiKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<MovieListResponse>() {
                    @Override
                    public void onNext(MovieListResponse movieListResponse) {
                        view.onSuccess(movieListResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError();
                    }

                    @Override
                    public void onComplete() { }
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void findMovie(String query) {
        moviesService.findMovies(query, new Constants().apiKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<MovieListResponse>() {
                    @Override
                    public void onNext(MovieListResponse movieListResponse) {
                        view.onSuccess(movieListResponse);
                    }

                    @Override
                    public void onError(Throwable e) { }

                    @Override
                    public void onComplete() { }
                });
    }
}
