package com.rafaelfelipeac.movies.movies;

import com.rafaelfelipeac.movies.model.MovieListResponse;

public interface MoviesContract {
    interface View {
        void onSuccess(MovieListResponse movieListResponse);
        void onError();
    }

    interface Presenter {
        void getMovies();
        void findMovie(String query);
        void setView(MoviesContract.View view);
    }
}
