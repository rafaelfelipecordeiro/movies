package com.rafaelfelipeac.movies.movies;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import com.rafaelfelipeac.movies.R;
import com.rafaelfelipeac.movies.adapter.CardViewAdapter;
import com.rafaelfelipeac.movies.adapter.RecyclerViewClickPosition;
import com.rafaelfelipeac.movies.details.DetailsActivity;
import com.rafaelfelipeac.movies.model.Movie;
import com.rafaelfelipeac.movies.model.MovieListResponse;
import com.rafaelfelipeac.movies.service.Constants;
import com.rafaelfelipeac.movies.service.MoviesService;
import com.rafaelfelipeac.movies.service.RetrofitClient;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesActivity extends AppCompatActivity implements MoviesContract.View, RecyclerViewClickPosition {

    private MoviesPresenter presenter = new MoviesPresenter(
            new RetrofitClient().getClient().create(MoviesService.class));

    private List<Movie> movies = new ArrayList();

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @BindView(R.id.movies_recyclerview)
    RecyclerView listDreams;
    @BindView(R.id.movies_progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        listDreams.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        listDreams.setLayoutManager(mLayoutManager);

        presenter.setView(this);
        presenter.getMovies();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchElementListeners(item);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) { }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(MovieListResponse movieListResponse) {
        progressBarVisibility(false);
        updateMovies(movieListResponse);
    }

    @Override
    public void  onError() {
        createDialog(getString(R.string.dialog_error_api));
        progressBarVisibility(false);
    }

    private void searchElementListeners(MenuItem item) {
        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                cleanList();
                presenter.findMovie(newText);
                return false;
            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                presenter.getMovies();
                return false;
            }
        });
    }

    private void cleanList() {
        List<Movie> l = new ArrayList<>();

        mAdapter = new CardViewAdapter(l, this);
        listDreams.setAdapter(mAdapter);
    }

    private void progressBarVisibility(boolean visibility) {
        if (visibility) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void updateMovies(MovieListResponse movieListResponse) {
       movies = new ArrayList<>();

       for(Movie m: movieListResponse.results) {
           if (m.getVote_average() > 5) {
               movies.add(m);
           }
       }

       mAdapter = new CardViewAdapter(movies, this);
       listDreams.setAdapter(mAdapter);
    }

    @Override
    public void getRecyclerViewAdapterPosition(int position) {
        Movie movie = movies.get(position);

        Intent intentDetailsActivity = new Intent(MoviesActivity.this, DetailsActivity.class);
        intentDetailsActivity.putExtra(new Constants().BUNDLE_DETAILS, movie);
        startActivity(intentDetailsActivity);
    }

    public void createDialog(String message) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        progressBarVisibility(false);
                        dialog.cancel();
                    }
                }).create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.
                getColor(this, R.color.colorPrimaryDark));
    }
}
