package com.rafaelfelipeac.movies.service;

import okhttp3.OkHttpClient.Builder;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public final class RetrofitClient {
    private Retrofit retrofit;

    public final Retrofit getClient() {
        if (retrofit == null) {
            Builder httpClient = this.setLogging();

            retrofit = (new retrofit2.Retrofit.Builder())
                    .baseUrl(new Constants().baseUrl)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }

        return this.retrofit;
    }

    private Builder setLogging() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(Level.BODY);
        Builder httpClient = new Builder();
        httpClient.addInterceptor(logging);
        return httpClient;
    }
}
