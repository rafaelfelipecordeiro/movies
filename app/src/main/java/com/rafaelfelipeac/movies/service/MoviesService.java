package com.rafaelfelipeac.movies.service;

import com.rafaelfelipeac.movies.model.MovieListResponse;
import com.rafaelfelipeac.movies.model.MovieResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MoviesService {

    @GET("movie/now_playing")
    Observable<MovieListResponse> getMovies(@Query("api_key") String apikey);

    @GET("movie/{id}")
    Observable<MovieResponse> getMovieDetails(@Path("id") int movieID, @Query("api_key") String apiKey);

    @GET("search/movie")
    Observable<MovieListResponse> findMovies(@Query("query") String query, @Query("api_key") String apiKey);
}
