package com.rafaelfelipeac.movies.service;

public class Constants {
    public static final String BUNDLE_DETAILS = "movie_details";

    public String baseUrl = "https://api.themoviedb.org/3/";
    public String imagesURL = "https://image.tmdb.org/t/p/w500/";

    public String apiKey = "63b3c82fc2382602b2fc9163b0211d61";
}
