package com.rafaelfelipeac.movies.model;

import java.util.List;

public class MovieListResponse {
    public int page;
    public List<Movie> results;
    public MovieDates dates;
    public int total_pages;
    public int total_results;
}
