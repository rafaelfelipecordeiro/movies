package com.rafaelfelipeac.movies.details;

import com.rafaelfelipeac.movies.model.MovieResponse;

public interface DetailsContract {
    interface View {
        void onSuccess(MovieResponse movieResponse);
        void onError();
    }

    interface Presenter {
        void getMovieDetails(int movieId);
        void setView(DetailsContract.View view);
    }
}
