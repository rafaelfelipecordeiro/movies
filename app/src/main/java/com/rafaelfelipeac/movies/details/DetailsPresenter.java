package com.rafaelfelipeac.movies.details;

import android.annotation.SuppressLint;
import com.rafaelfelipeac.movies.model.MovieResponse;
import com.rafaelfelipeac.movies.service.Constants;
import com.rafaelfelipeac.movies.service.MoviesService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class DetailsPresenter implements DetailsContract.Presenter {

    private MoviesService moviesService;
    private DetailsContract.View view;

    DetailsPresenter(MoviesService moviesService) {
        this.moviesService = moviesService;
    }

    @Override
    public void setView(DetailsContract.View view) {
        this.view = view;
    }

    @SuppressLint("CheckResult")
    @Override
    public void getMovieDetails(int movieId) {
        moviesService.getMovieDetails(movieId, new Constants().apiKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<MovieResponse>() {
                    @Override
                    public void onNext(MovieResponse movieResponse) {
                        view.onSuccess(movieResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onError();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
