package com.rafaelfelipeac.movies.details;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.rafaelfelipeac.movies.R;
import com.rafaelfelipeac.movies.model.Movie;
import com.rafaelfelipeac.movies.model.MovieResponse;
import com.rafaelfelipeac.movies.service.Constants;
import com.rafaelfelipeac.movies.service.MoviesService;
import com.rafaelfelipeac.movies.service.RetrofitClient;
import com.squareup.picasso.Picasso;
import java.text.NumberFormat;
import java.util.Locale;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsActivity extends AppCompatActivity implements DetailsContract.View {

    private DetailsPresenter presenter = new DetailsPresenter(
            new RetrofitClient().getClient().create(MoviesService.class));

    Movie movie;

    @BindView(R.id.details_title)
    TextView title;
    @BindView(R.id.details_desc)
    TextView desc;
    @BindView(R.id.details_score)
    TextView score;
    @BindView(R.id.details_votes)
    TextView votes;
    @BindView(R.id.details_image)
    ImageView image;
    @BindView(R.id.details_releaseDate)
    TextView releaseDate;
    @BindView(R.id.details_homepage)
    TextView homepage;
    @BindView(R.id.details_budget)
    TextView budget;
    @BindView(R.id.details_revenue)
    TextView revenue;
    @BindView(R.id.details_progressBar)
    ProgressBar progressBar;
    @BindView(R.id.details_content)
    View detailsContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        movie = (Movie) getIntent().getSerializableExtra(new Constants().BUNDLE_DETAILS);

        presenter.setView(this);
        presenter.getMovieDetails(movie.getId());
    }

    @SuppressLint("SetTextI18n")
    private void updateLayout(MovieResponse movieResponse) {
        title.setText(movieResponse.getTitle());
        desc.setText(movieResponse.getOverview());
        score.setText(movieResponse.getVote_average().toString());
        votes.setText(movieResponse.getVote_count().toString() + " votes");
        releaseDate.setText(movieResponse.getRelease_date().replace("-", "/"));
        homepage.setText(movieResponse.getHomepage());

        Locale locale = new Locale("en", "US");
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);

        budget.setText(currencyFormatter.format(movieResponse.getBudget()));
        revenue.setText(currencyFormatter.format(movieResponse.getRevenue()));

        Picasso.get()
                .load(new Constants().imagesURL + movieResponse.getBackdrop_path())
                .error(R.drawable.placeholder_image)
                .into(image);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(MovieResponse movieResponse) {
        updateLayout(movieResponse);
        progressBarVisibility(false);
    }

    @Override
    public void onError() {
        createDialog(getString(R.string.dialog_error_api));
        progressBarVisibility(false);
    }

    private void progressBarVisibility(boolean visibility) {
        if (visibility) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
            detailsContent.setVisibility(View.VISIBLE);
        }
    }

    public void createDialog(String message) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        progressBarVisibility(false);
                        dialog.cancel();
                    }
                }).create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.
                getColor(this, R.color.colorPrimaryDark));
    }
}
