package com.rafaelfelipeac.movies.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.rafaelfelipeac.movies.R;
import com.rafaelfelipeac.movies.model.Movie;
import com.rafaelfelipeac.movies.service.Constants;
import com.squareup.picasso.Picasso;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.ViewHolder> {

    public Movie[] movies;
    private static RecyclerViewClickPosition mPositionInterface;

    @BindView(R.id.item_title)
    TextView title;
    @BindView(R.id.item_desc)
    TextView desc;
    @BindView(R.id.item_date)
    TextView date;
    @BindView(R.id.item_image)
    ImageView image;

    public CardViewAdapter(List<Movie> movies, RecyclerViewClickPosition positionInterface) {
        this.movies = movies.toArray(new Movie[movies.size()]);
        mPositionInterface = positionInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, null);
        ButterKnife.bind(this, itemLayoutView);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        ButterKnife.bind(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        title.setText(movies[position].getTitle());
        date.setText(movies[position].getRelease_date().replace("-", "/"));
        desc.setText(movies[position].getOverview());

        viewHolder.setIsRecyclable(false);

        Picasso.get()
                .load(new Constants().imagesURL + movies[position].getPoster_path())
                .error(R.drawable.placeholder_image)
                .into(image);
    }

    @Override
    public int getItemCount() {
        return movies.length;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            itemLayoutView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mPositionInterface.getRecyclerViewAdapterPosition(this.getLayoutPosition());
        }
    }
}