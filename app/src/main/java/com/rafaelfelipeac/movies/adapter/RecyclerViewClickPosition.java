package com.rafaelfelipeac.movies.adapter;

public interface RecyclerViewClickPosition {
    void getRecyclerViewAdapterPosition(int position);
}