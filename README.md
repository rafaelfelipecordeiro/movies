# movies-exercise

# About  
A minimalist app for discover some movies.
  
## Libraries  
- Retrofit  
- RxJava2  
- ButterKnife
- Picasso
- DiagonalLayout
  
## Setup  
1. Clone the repository from https://bitbucket.org/rafaelfelipecordeiro/movies/src/master.  
2. Load the project in Android Studio.  

## Compile  
1. Do a gradle sync to download the project dependencies.  
  
## Run  
1. In Android Studio, hit run, select the device to deploy and wait for the project to compile.
